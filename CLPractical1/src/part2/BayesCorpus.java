package part2;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import part1.Tokeniser;

/**
 * Given a directory containing POS data or a set of training files, provides methods to build various
 * frequency matrices.
 * 
 * @author Siddharth
 *
 */
public class BayesCorpus {
	
	// Set of files to build training corpus from
	private Set<File> trainFiles;
	
	// Set of tags
	private Set<String> tagSet;
	
	// Set of words in the corpus
	private Set<String> wordSet;
	
	// List of words as feature vectors
	private List<Word> corpus;
	
	// Map of tag occurrences.
	private Map<String, Integer> tagFrequency;
	
	// Map (2D associative array) of frequency of previous tag given current tag
	private Map<String, Map<String, Integer>> prevTagFrequency;
	
	// Map (2D associative array) of frequency of next tag given current tag
	private Map<String, Map<String, Integer>> nextTagFrequency;
	
	// Map (2D associative array) of frequency of current word given current tag
	private Map<String, Map<String, Integer>> wordFrequency;
	
	// Map (2D associative array) of frequency of current word being plural given current tag
	private Map<String, Map<Boolean, Integer>> pluralityFrequency;
	
	// Map (2D associative array) of frequency of current word starting with upper case given current tag
	private Map<String, Map<Boolean, Integer>> startingCaseFrequency;
	
	// Map (2D associative array) of frequency of current word starting with upper case and being 
	// starting word of the sentence given current tag
	private Map<String, Map<Boolean, Integer>> startingCaseStartingSentenceFrequency;
	
	// Map (2D associative array) of frequency of current word being in upper case given current tag
	private Map<String, Map<Boolean, Integer>> upperCaseFrequency;
	
	// Map (2D associative array) of frequency of current word being alphanumeric given current tag
	private Map<String, Map<Boolean, Integer>> alphanumericFrequency;

	public BayesCorpus(Set<File> files) {
		trainFiles = files;
		if (files == null || files.size() == 0)
			throw new NullPointerException("No training files received");
		corpus = new ArrayList<Word>();
		tagFrequency = new HashMap<String, Integer>();
		prevTagFrequency = new HashMap<String, Map<String,Integer>>();
		nextTagFrequency = new HashMap<String, Map<String,Integer>>();
		wordFrequency = new HashMap<String, Map<String,Integer>>();
		pluralityFrequency = new HashMap<String, Map<Boolean,Integer>>();
		startingCaseFrequency = new HashMap<String, Map<Boolean,Integer>>();
		startingCaseStartingSentenceFrequency = new HashMap<String, Map<Boolean,Integer>>();
		upperCaseFrequency = new HashMap<String, Map<Boolean,Integer>>();
		alphanumericFrequency = new HashMap<String, Map<Boolean,Integer>>();
		wordSet = new HashSet<String>();
		tagSet = new HashSet<String>();
		tagSet.add(Tokeniser.START_TAG);
		tagSet.add(Tokeniser.END_TAG);
	}
	/**
	 * Trains the corpus from all the training files as a <tt>List</tt> of words
	 * represented as feature vectors
	 * @throws IOException
	 */
	public void train() throws IOException {
		for (File file : trainFiles) {
			Tokeniser tokeniser = new Tokeniser(file);
			tokeniser.tokeniseAll();
			List<String> words = tokeniser.getWords();
			List<String> tags = tokeniser.getTags();
			
			for (int i = 0; i < words.size(); i++) {
				
				// Add tag to tagSet
				if (!tagSet.contains(tags.get(i)))
					tagSet.add(tags.get(i));

				// Add word to wordSet
				if (!wordSet.contains(words.get(i)))
					wordSet.add(words.get(i));
				
				boolean startUpperCase = false;
				String previousTag = "";
				String nextTag = "";
				String currentTag = tags.get(i);
				if (i == 0 || tags.get(i-1).equals(Tokeniser.END_TAG)) {
					previousTag = Tokeniser.START_TAG;
					startUpperCase = true;
				}
				if (i == words.size() - 1)
					nextTag = Tokeniser.START_TAG;
				Word word = new Word(words.get(i), (previousTag.equals("") ? tags.get(i-1) : previousTag), (nextTag.equals("") ? tags.get(i+1) : nextTag), startUpperCase);
				word.setTag(currentTag);
				corpus.add(word);
				updateTagFrequency(currentTag);
				updateWordFrequency(currentTag, words.get(i));
				updatePrevTagFrequency(currentTag, previousTag);
				updateNextTagFrequency(currentTag, nextTag);
				updatePluralityFrequency(currentTag, word.isPlural);
				updateStartingCaseFrequency(currentTag, word.isStartingCase);
				updateStartingCaseStartingSentenceFrequency(currentTag, word.isStartingCaseStartingSentence);
				updateUpperCaseFrequency(currentTag, word.isUpperCase);
				updateAlphanumericFrequency(currentTag, word.isAlphaNumeric);
			}
		}
	}
	
	/**
	 * Updates the number of occurrences of current tag in the corpus by 1 
	 * @param currentTag
	 */
	private void updateTagFrequency(String currentTag) {
		if (!tagFrequency.containsKey(currentTag))
			tagFrequency.put(currentTag, 1);
		else
			tagFrequency.put(currentTag, tagFrequency.get(currentTag)+1);
	}
	
	/**
	 * Updates the number of occurrences of previous tag, given current in the corpus by 1 
	 * @param currentTag
	 * @param prevTag
	 */
	private void updatePrevTagFrequency(String currentTag, String prevTag) {
		if (!prevTagFrequency.containsKey(currentTag)) {
			Map<String, Integer> internalMap = new HashMap<String, Integer>();
			internalMap.put(prevTag, 1);
			prevTagFrequency.put(currentTag, internalMap);
		}
		else {
			Map<String, Integer> internalMap = prevTagFrequency.get(currentTag);
			if (!internalMap.containsKey(prevTag))
				internalMap.put(prevTag, 1);
			else
				internalMap.put(prevTag, internalMap.get(prevTag) + 1);
			prevTagFrequency.put(currentTag, internalMap);
		}
	}
	
	/**
	 * Updates the number of occurrences of next tag, given current in the corpus by 1 
	 * @param currentTag
	 * @param nextTag
	 */
	private void updateNextTagFrequency(String currentTag, String nextTag) {
		if (!nextTagFrequency.containsKey(currentTag)) {
			Map<String, Integer> internalMap = new HashMap<String, Integer>();
			internalMap.put(nextTag, 1);
			nextTagFrequency.put(currentTag, internalMap);
		}
		else {
			Map<String, Integer> internalMap = nextTagFrequency.get(currentTag);
			if (!internalMap.containsKey(nextTag))
				internalMap.put(nextTag, 1);
			else
				internalMap.put(nextTag, internalMap.get(nextTag) + 1);
			nextTagFrequency.put(currentTag, internalMap);
		}
	}
	
	/**
	 * Updates the number of occurrences of current word, given current tag in the corpus by 1 
	 * @param currentTag
	 * @param word
	 */
	private void updateWordFrequency(String currentTag, String word) {
		if (!wordFrequency.containsKey(currentTag)) {
			Map<String, Integer> internalMap = new HashMap<String, Integer>();
			internalMap.put(word, 1);
			wordFrequency.put(currentTag, internalMap);
		}
		else {
			Map<String, Integer> internalMap = wordFrequency.get(currentTag);
			if (!internalMap.containsKey(word))
				internalMap.put(word, 1);
			else
				internalMap.put(word, internalMap.get(word) + 1);
			wordFrequency.put(currentTag, internalMap);
		}
	}
	
	/**
	 * Updates the number of occurrences of plurality of current word, given current tag in the corpus by 1
	 * @param currentTag
	 * @param plural
	 */
	private void updatePluralityFrequency(String currentTag, boolean plural) {
		if (!pluralityFrequency.containsKey(currentTag)) {
			Map<Boolean, Integer> internaplMap = new HashMap<Boolean, Integer>();
			internaplMap.put(plural, 1);
			pluralityFrequency.put(currentTag, internaplMap);
		}
		else {
			Map<Boolean, Integer> internalMap = pluralityFrequency.get(currentTag);
			if (!internalMap.containsKey(plural))
				internalMap.put(plural, 1);
			else
				internalMap.put(plural, internalMap.get(plural) + 1);
			pluralityFrequency.put(currentTag, internalMap);
		}
	}
	
	/**
	 * Updates the number of occurrences of starting case being upper case of current word, 
	 * given current tag in the corpus by 1
	 * @param currentTag
	 * @param startingCase
	 */
	private void updateStartingCaseFrequency(String currentTag, boolean startingCase) {
		if (!startingCaseFrequency.containsKey(currentTag)) {
			Map<Boolean, Integer> internaplMap = new HashMap<Boolean, Integer>();
			internaplMap.put(startingCase, 1);
			startingCaseFrequency.put(currentTag, internaplMap);
		}
		else {
			Map<Boolean, Integer> internalMap = startingCaseFrequency.get(currentTag);
			if (!internalMap.containsKey(startingCase))
				internalMap.put(startingCase, 1);
			else
				internalMap.put(startingCase, internalMap.get(startingCase) + 1);
			startingCaseFrequency.put(currentTag, internalMap);
		}
	}
	
	/**
	 * Updates the number of occurrences of starting case being upper case of current word, 
	 * and the current word being starting word of the sentence given current tag in the corpus by 1
	 * @param currentTag
	 * @param startingCaseStartingSentence
	 */
	private void updateStartingCaseStartingSentenceFrequency(String currentTag, boolean startingCaseStartingSentence) {
		if (!startingCaseStartingSentenceFrequency.containsKey(currentTag)) {
			Map<Boolean, Integer> internaplMap = new HashMap<Boolean, Integer>();
			internaplMap.put(startingCaseStartingSentence, 1);
			startingCaseStartingSentenceFrequency.put(currentTag, internaplMap);
		}
		else {
			Map<Boolean, Integer> internalMap = startingCaseStartingSentenceFrequency.get(currentTag);
			if (!internalMap.containsKey(startingCaseStartingSentence))
				internalMap.put(startingCaseStartingSentence, 1);
			else
				internalMap.put(startingCaseStartingSentence, internalMap.get(startingCaseStartingSentence) + 1);
			startingCaseStartingSentenceFrequency.put(currentTag, internalMap);
		}
	}
	
	/**
	 * Updates the number of occurrences of current word being in upper case, given current tag in the corpus by 1
	 * @param currentTag
	 * @param upperCase
	 */
	private void updateUpperCaseFrequency(String currentTag, boolean upperCase) {
		if (!upperCaseFrequency.containsKey(currentTag)) {
			Map<Boolean, Integer> internaplMap = new HashMap<Boolean, Integer>();
			internaplMap.put(upperCase, 1);
			upperCaseFrequency.put(currentTag, internaplMap);
		}
		else {
			Map<Boolean, Integer> internalMap = upperCaseFrequency.get(currentTag);
			if (!internalMap.containsKey(upperCase))
				internalMap.put(upperCase, 1);
			else
				internalMap.put(upperCase, internalMap.get(upperCase) + 1);
			upperCaseFrequency.put(currentTag, internalMap);
		}
	}
	
	/**
	 * Updates the number of occurrences of current word being alphanumeric, given current tag in the corpus by 1
	 * @param currentTag
	 * @param alphanumeric
	 */
	private void updateAlphanumericFrequency(String currentTag, boolean alphanumeric) {
		if (!alphanumericFrequency.containsKey(currentTag)) {
			Map<Boolean, Integer> internaplMap = new HashMap<Boolean, Integer>();
			internaplMap.put(alphanumeric, 1);
			alphanumericFrequency.put(currentTag, internaplMap);
		}
		else {
			Map<Boolean, Integer> internalMap = alphanumericFrequency.get(currentTag);
			if (!internalMap.containsKey(alphanumeric))
				internalMap.put(alphanumeric, 1);
			else
				internalMap.put(alphanumeric, internalMap.get(alphanumeric) + 1);
			alphanumericFrequency.put(currentTag, internalMap);
		}
	}
	
	/**
	 * Returns the probability of occurrence of a given tag
	 * @param tag the given tag
	 * @return the probability of occurrence
	 */
	public double getTagProbability(String tag) {
		double probability = 0;
		int occurrences = 0;
		if (tagFrequency.containsKey(tag))
			occurrences = tagFrequency.get(tag);
		probability = Math.log(occurrences) - Math.log(corpus.size());
		probability = Math.pow(Math.E, probability);
		return (occurrences == 0 ? 0.00000001 : probability);
	}
	
	/**
	 * Returns the probability of occurrence of previous tag given current tag
	 * @param currentTag
	 * @param prevTag
	 * @return
	 */
	public double getPreviousTagProbability(String currentTag, String prevTag) {
		double probability = 0;
		int occurrences = 0;
		int size = 0;
		if (prevTagFrequency.containsKey(currentTag))
			if (prevTagFrequency.get(currentTag).containsKey(prevTag))
				occurrences = prevTagFrequency.get(currentTag).get(prevTag);
		if (tagFrequency.containsKey(currentTag))
			size = tagFrequency.get(currentTag);
		probability = Math.log(occurrences) - Math.log(size);
		probability = Math.pow(Math.E, probability);
		return (size == 0 && occurrences == 0 ? 0.00000001 : probability);
	}
	
	/**
	 * Returns the probability of occurrence of next tag given current tag
	 * @param currentTag
	 * @param nextTag
	 * @return
	 */
	public double getNextTagProbability(String currentTag, String nextTag) {
		double probability = 0;
		int occurrences = 0;
		int size = 0;
		if (nextTagFrequency.containsKey(currentTag))
			if (nextTagFrequency.get(currentTag).containsKey(nextTag))
				occurrences = nextTagFrequency.get(currentTag).get(nextTag);
		if (tagFrequency.containsKey(currentTag))
			size = tagFrequency.get(currentTag);
		probability = Math.log(occurrences) - Math.log(size);
		probability = Math.pow(Math.E, probability);
		return (size == 0 && occurrences == 0 ? 0.00000001 : probability);
	}
	
	/**
	 * Returns the probability of occurrence of the word given current tag
	 * @param currentTag
	 * @param word
	 * @return
	 */
	public double getWordProbability(String currentTag, String word) {
		double probability = 0;
		int occurrences = 0;
		int size = 0;
		if (wordFrequency.containsKey(currentTag))
			if (wordFrequency.get(currentTag).containsKey(word))
				occurrences = wordFrequency.get(currentTag).get(word);
		if (tagFrequency.containsKey(currentTag))
			size = tagFrequency.get(currentTag);
		probability = Math.log(occurrences) - Math.log(size);
		probability = Math.pow(Math.E, probability);
		return (size == 0 && occurrences == 0 ? 0.00000001 : probability);
	}
	
	/**
	 * Returns the probability of occurrence of the word being plural given current tag
	 * @param currentTag
	 * @param plurality
	 * @return
	 */
	public double getPluralityProbability(String currentTag, boolean plurality) {
		double probability = 0;
		int occurrences = 0;
		int size = 0;
		if (pluralityFrequency.containsKey(currentTag))
			if (pluralityFrequency.get(currentTag).containsKey(plurality))
				occurrences = pluralityFrequency.get(currentTag).get(plurality);
		if (tagFrequency.containsKey(currentTag))
			size = tagFrequency.get(currentTag);
		probability = Math.log(occurrences) - Math.log(size);
		probability = Math.pow(Math.E, probability);
		return (size == 0 && occurrences == 0 ? 0.00000001 : probability);
	}
	
	/**
	 * Returns the probability of occurrence of the word starting with upper case given current tag
	 * @param currentTag
	 * @param startingCase
	 * @return
	 */
	public double getStartingCaseProbability(String currentTag, boolean startingCase) {
		double probability = 0;
		int occurrences = 0;
		int size = 0;
		if (startingCaseFrequency.containsKey(currentTag))
			if (startingCaseFrequency.get(currentTag).containsKey(startingCase))
				occurrences = startingCaseFrequency.get(currentTag).get(startingCase);
		if (tagFrequency.containsKey(currentTag))
			size = tagFrequency.get(currentTag);
		probability = Math.log(occurrences) - Math.log(size);
		probability = Math.pow(Math.E, probability);
		return (size == 0 && occurrences == 0 ? 0.00000001 : probability);
	}
	
	/**
	 * Returns the probability of occurrence of the word being start of sentence and starting 
	 * with upper case given current tag
	 * @param currentTag
	 * @param startingCaseStartingSentence
	 * @return
	 */
	public double getStartingCaseStartingSentenceProbability(String currentTag, boolean startingCaseStartingSentence) {
		double probability = 0;
		int occurrences = 0;
		int size = 0;
		if (startingCaseStartingSentenceFrequency.containsKey(currentTag))
			if (startingCaseStartingSentenceFrequency.get(currentTag).containsKey(startingCaseStartingSentence))
				occurrences = startingCaseStartingSentenceFrequency.get(currentTag).get(startingCaseStartingSentence);
		if (tagFrequency.containsKey(currentTag))
			size = tagFrequency.get(currentTag);
		probability = Math.log(occurrences) - Math.log(size);
		probability = Math.pow(Math.E, probability);
		return (size == 0 && occurrences == 0 ? 0.00000001 : probability);
	}
	
	/**
	 * Returns the probability of occurrence of the word being upper case given current tag
	 * @param currentTag
	 * @param upperCase
	 * @return
	 */
	public double getUpperCaseProbability(String currentTag, boolean upperCase) {
		double probability = 0;
		int occurrences = 0;
		int size = 0;
		if (upperCaseFrequency.containsKey(currentTag))
			if (upperCaseFrequency.get(currentTag).containsKey(upperCase))
				occurrences = upperCaseFrequency.get(currentTag).get(upperCase);
		if (tagFrequency.containsKey(currentTag))
			size = tagFrequency.get(currentTag);
		probability = Math.log(occurrences) - Math.log(size);
		probability = Math.pow(Math.E, probability);
		return (size == 0 && occurrences == 0 ? 0.00000001 : probability);
	}
	
	/**
	 * Returns the probability of occurrence of the word being alphanumeric given current tag
	 * @param currentTag
	 * @param alphanumeric
	 * @return
	 */
	public double getAlphanumericProbability(String currentTag, boolean alphanumeric) {
		double probability = 0;
		int occurrences = 0;
		int size = 0;
		if (alphanumericFrequency.containsKey(currentTag))
			if (alphanumericFrequency.get(currentTag).containsKey(alphanumeric))
				occurrences = alphanumericFrequency.get(currentTag).get(alphanumeric);
		if (tagFrequency.containsKey(currentTag))
			size = tagFrequency.get(currentTag);
		probability = Math.log(occurrences) - Math.log(size);
		probability = Math.pow(Math.E, probability);
		return (size == 0 && occurrences == 0 ? 0.00000001 : probability);
	}
	
	/**
	 * Returns the tag set of the corpus
	 * @return the tag set
	 */
	public Set<String> getTagSet() {
		return tagSet;
	}
}
