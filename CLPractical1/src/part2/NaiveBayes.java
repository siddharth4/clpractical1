package part2;

import java.util.List;

/**
 * Implementation of the Naive Bayes Algorithm
 * 
 * @author Siddharth
 *
 */
public class NaiveBayes {
	
	// Given sentence as a List of words
	private List<Word> words;

	// Tags assigned to words by Naive Bayes
	private String[] assignedTags;

	// List of tags, populated from given corpus
	private String[] tags;
	
	// Trained corpus
	private BayesCorpus corpus;
	
	public NaiveBayes(List<Word> sentence, BayesCorpus corpus) {
		if (sentence == null || corpus.getTagSet() == null || sentence.size() == 0 || corpus.getTagSet().size() == 0)
			throw new NullPointerException("Incorrect sentence or tag set");
		this.corpus = corpus;
		words = sentence;
		populateTags();
		assignedTags = new String[words.size()];
	}
	
	/**
	 * Converts tag set from corpus into a <tt>String</tt> array
	 */
	private void populateTags() {
		tags = new String[corpus.getTagSet().size()];
		int i = 0;
		for (String tag : corpus.getTagSet()) 
			tags[i++] = tag;
	}
	
	/**
	 * Executes the Naive Bayes algorithm
	 */
	public void execute() {
		for (int i = 0; i < words.size(); i++) {
			Word word = words.get(i);
			double max = 0;
			int index = 0;
			for (int j = 0; j < tags.length; j++) {
				double score = corpus.getTagProbability(tags[j]);
				score *= corpus.getWordProbability(tags[j], word.string);
				score *= corpus.getPreviousTagProbability(tags[j], word.previousTag);
				score *= corpus.getNextTagProbability(tags[j], word.nextTag);
				score *= corpus.getPluralityProbability(tags[j], word.isPlural);
				score *= corpus.getStartingCaseProbability(tags[j], word.isStartingCase);
				score *= corpus.getStartingCaseStartingSentenceProbability(tags[j], word.isStartingCaseStartingSentence);
				score *= corpus.getUpperCaseProbability(tags[j], word.isUpperCase);
				score *= corpus.getAlphanumericProbability(tags[j], word.isAlphaNumeric);
				if (score > max) {
					max = score;
					index = j;
				}
			}
			assignedTags[i] = tags[index];
		}
	}
	
	/**
	 * Returns the tags assigned to the given set of words by the Naive Bayes algorithm
	 * @return the tags assigned
	 */
	public String[] getAssignedTags() {
		return assignedTags;
	}

}
