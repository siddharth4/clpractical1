package part2;

/**
 * Representation of a word as a combination of features
 * 
 * @author Siddharth
 *
 */
public class Word {
	
	// The word itself
	protected String string;
	
	// Whether word is plural or not
	protected boolean isPlural;
	
	// Orthography: Starting case. Whether first letter is capital
	protected boolean isStartingCase;
	
	// Orthography: Starting case. Whether first letter is capital and start of sentence
	protected boolean isStartingCaseStartingSentence;
	
	// Orthography: Whether uppercase or not
	protected boolean isUpperCase;
	
	// Orthography: Whether alphanumneric or not
	protected boolean isAlphaNumeric;
	
	// POS tag of the word
	protected String tag;
	
	// POS tag of the previous word
	protected String previousTag;
	
	// POS tag of the next word
	protected String nextTag;
	
	public Word(String word, String previousTag, String nextTag, boolean isStartingCaseStartingSentence) {
		string = word;
		this.previousTag = previousTag;
		this.nextTag = nextTag;
		this.isStartingCaseStartingSentence = isStartingCaseStartingSentence;
		isPlural = checkPlurality(word);
		isStartingCase = checkStartingCase(word);
		isUpperCase = checkUpperCase(word);
		isAlphaNumeric = checkAlphanumeric(word);
	}
	
	private boolean checkStartingCase(String word) {
		if (word.length() == 0)
			return false;
		return Character.isUpperCase(word.charAt(0));
	}
	
	private boolean checkPlurality(String word) {
		if (word.length() == 0)
			return false;
		// Very basic way to check for plurality, prone to errors
		return word.endsWith("s") || word.endsWith("ies");
	}
	
	private boolean checkAlphanumeric(String word) {
		if (word.length() == 0)
			return false;
		return word.matches("[a-zA-Z0-9]+");
	}
	
	private boolean checkUpperCase(String word) {
		if (word.length() == 0)
			return false;
		return word.equals(word.toUpperCase());
	}
	
	public void setTag(String tag) {
		this.tag = tag;
	}
	
	public String getTag() {
		return tag;
	}

}
