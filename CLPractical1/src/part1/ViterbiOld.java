package part1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ViterbiOld {
	
	// Score for each given <tag><word> pair
	private Map<String, Map<String, Double>> score;
	
	// Backpointer for each given <tag><word> pair	
	private Map<String, Map<String, Integer>> backpointer;
	
	// Given sentence as a List of words
	private List<String> words;
	
	// Tags assigned to words by Viterbi
	//private List<String> assignedTags;
	
	// List of tags, populated from given corpus
	private List<String> tags;
	
	// Given corpus
	private Corpus corpus;
	private String[] assignedArray;
	
	public ViterbiOld(List<String> sentence, Corpus corpus) {
		if (sentence == null || corpus.getTagSet() == null || sentence.size() == 0 || corpus.getTagSet().size() == 0)
			throw new NullPointerException("Incorrect sentence or tag set");
		this.corpus = corpus;
		words = getWords(sentence);
		score = new HashMap<String, Map<String,Double>>();
		backpointer = new HashMap<String, Map<String,Integer>>();
		tags = new ArrayList<String>();
//		initialiseAssignedTags();
//		assignedTags = new ArrayList<String>(words.size());
		assignedArray = new String[words.size()];
		populateTags();
	}
	
	/**
	 * Converts tag set from corpus into an iterable <tt>List</tt>
	 */
	private void populateTags() {
		for (String tag : corpus.getTagSet())
			tags.add(tag);
	}
	
	/**
	 * Initialises the <tt>assignedTags</tt> to a <tt>List</tt> of START_TAG 
	 * Must only be called after <tt>getWords()</tt> so that size of <tt>words</tt> and 
	 * <tt>assignedTags</tt> matches
	 * @return
	 */
	/*private void initialiseAssignedTags() {
		assignedTags = new ArrayList<String>();
		for (int i = 0; i < words.size(); i++)
			assignedTags.add(Tokeniser.START_TAG);
	}*/
	
	/**
	 * Adds START_WORD to the start of the sentence and returns a <tt>List</tt> of words
	 * @param sentence the given sentence
	 * @return the <tt>List</tt> of words with START_WORD
	 */
	private List<String> getWords(List<String> sentence) {
		List<String> wordsWithStartTag = new ArrayList<String>();
		wordsWithStartTag.add(Tokeniser.START_WORD);
		for (String word : sentence)
			wordsWithStartTag.add(word);
		return wordsWithStartTag;
	}
	
	/**
	 * Execute the Viterbi algorithm
	 */
	public void execute() {
		
		// Initialise
		for (int i = 0; i < tags.size(); i++) {
			Map<String, Double> internalMap = new HashMap<String, Double>();
			double value = corpus.getTagProbability(tags.get(i), words.get(0));
			value *= corpus.getBigramProbability(Tokeniser.START_TAG, tags.get(i));
			internalMap.put(words.get(0), value);
			score.put(tags.get(i), internalMap);
		}
		
		// Induction
		for (int j = 1; j < words.size(); j++) {
			for (int i = 0; i < tags.size(); i++) {
				double max = Double.MIN_VALUE;
				int backK = 0;
				for (int k = 0; k < tags.size(); k++) {
					if (score.containsKey(tags.get(k)) && score.get(tags.get(k)).containsKey(words.get(j-1))) {
						double value = score.get(tags.get(k)).get(words.get(j-1));
						value *= corpus.getBigramProbability(tags.get(k), tags.get(i));
						value *= corpus.getTagProbability(tags.get(i), words.get(j));
						if (max < value) {
							max = value;
							backK = k;
						}
					}
				}
				Map<String, Double> internalMap = new HashMap<String, Double>();
				internalMap.put(words.get(j), max);
				score.put(tags.get(i), internalMap);
				
				Map<String, Integer> newInternalMap = new HashMap<String, Integer>();
				newInternalMap.put(words.get(j), backK);
				backpointer.put(tags.get(i), newInternalMap);
			}
		}
		
		// Back tracing the best tagging
		double max = Double.MIN_VALUE;
		int index = 0, counter = 0;
		for (Map.Entry<String, Map<String, Double>> entry : score.entrySet()) {
			Map<String, Double> internalMap = entry.getValue();
			double value = 0;
			if (internalMap.containsKey(words.get(words.size()-1)))
				value = internalMap.get(words.get(words.size()-1));
			if (max < value) {
				max = value;
				index = counter;
			}
			counter++;
		}
		System.out.println(words.size()-1);
//		assignedTags.add(words.size()-1, tags.get(index));
		assignedArray[words.size()-1] = tags.get(index);
		for (int i = words.size()-2; i >= 0; i--) {
			int k = 0;
			if (backpointer.containsKey(tags.get(i+1)) && backpointer.get(tags.get(i+1)).containsKey(words.get(i+1)))
				backpointer.get(tags.get(i+1)).get(words.get(i+1));
//			assignedTags.add(i, tags.get(k));
			assignedArray[i] = tags.get(k);
		}
	}
	
	/**
	 * Prints the assigned tags to the given set of words
	 */
	public void printTags() {
		for (String word : words)
			System.out.print(word + "\t");
		System.out.println();
		for (String tag : assignedArray)
			System.out.print(tag + "\t");
	}

}
