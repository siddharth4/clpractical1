package part1;

import java.io.IOException;
import java.nio.file.NotDirectoryException;

import part2.POSTagBayesValidator;

public class Main {
	
	public static void main(String[] args) {
		
		String homeDir = "/auto/users/ms14s/Downloads/WSJ-2-12/";
		try {
//			POSTagValidator p = new POSTagValidator(homeDir, "-cross");
//			p.validate();
//			p = new POSTagValidator(homeDir, "-full");
//			p.validate();
			POSTagBayesValidator n = new POSTagBayesValidator(homeDir, "-cross");
			n.validate();
//			Corpus c = new Corpus(homeDir);
//			c.train();
//			System.out.println("Created corpus");
//			String[] sentence = {"This", "is", "a", "good", "sentence"};
//			Viterbi viterbi = new Viterbi(sentence, c);
//			viterbi.execute();
//			viterbi.printTags();
		} catch (NotDirectoryException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
