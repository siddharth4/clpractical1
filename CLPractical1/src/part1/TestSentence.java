package part1;

import java.util.ArrayList;
import java.util.List;

import part2.Word;

/**
 * Represents a test sentence and tags pair on which to run the Viterbi/Naive Bayes algorithm
 */
public class TestSentence {
	
	// The list of words
	private List<String> sentence;
	
	// The list of assigned tags
	private List<String> actualTags;
	
	public TestSentence(List<String> sentence, List<String> actualTags) {
		if (sentence == null || actualTags == null || sentence.size() != actualTags.size() || sentence.size() == 0 || actualTags.size() == 0)
			throw new NullPointerException("Incorrect sentence or assigned tags");
		this.sentence = sentence;
		this.actualTags = actualTags;
	}
	
	/**
	 * Returns the sentence as a <tt>String</tt> array to be used in Viterbi
	 * @return the <tt>String</tt> array of words
	 */
	public String[] getSentence() {
		return sentence.toArray(new String[sentence.size()]);
	}
	
	/**
	 * Returns the assigned tags as a <tt>String</tt> array to be used in Viterbi
	 * @return the <tt>String</tt> array of assigned tags
	 */
	public String[] getActualTags() {
		return actualTags.toArray(new String[actualTags.size()]);
	}
	
	/**
	 * Returns the sentence as a <tt>List</tt> of <tt>Word</tt> to be used in Naive Bayes
	 * @return the <tt>List</tt> of <tt>Word</tt>s
	 */
	public List<Word> getWords() {
		List<Word> words = new ArrayList<Word>();
		for (int i = 0; i < sentence.size(); i++) {
			boolean startUpperCase = false;
			String previousTag = "";
			String nextTag = "";
			String currentTag = actualTags.get(i);
			if (i == 0 || actualTags.get(i-1).equals(Tokeniser.END_TAG)) {
				previousTag = Tokeniser.START_TAG;
				startUpperCase = true;
			}
			if (i == sentence.size() - 1)
				nextTag = Tokeniser.START_TAG;
			Word word = new Word(sentence.get(i), (previousTag.equals("") ? actualTags.get(i-1) : previousTag), (nextTag.equals("") ? actualTags.get(i+1) : nextTag), startUpperCase);
			word.setTag(currentTag);
			words.add(word);
		}
		return words;
	}

}
