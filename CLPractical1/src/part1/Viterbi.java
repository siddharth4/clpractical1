package part1;

/**
 * Implementation of the Viterbi Algorithm
 * 
 * @author Siddharth
 *
 */
public class Viterbi {
	
	// Score for each given <tag><word> pair
	private double[][] score;
	
	// Backpointer for each given <tag><word> pair	
	private int[][] backpointer;
	
	// Given sentence as a String array of words
	private String[] words;
	
	// Tags assigned to words by Viterbi
	private String[] assignedTags;
	
	// Tags index assigned to words
	private int[] assignedTagIndex;
	
	// List of tags, populated from given corpus
	private String[] tags;
	
	// Trained corpus
	private Corpus corpus;
	
	public Viterbi(String[] sentence, Corpus corpus) {
		if (sentence == null || corpus.getTagSet() == null || sentence.length == 0 || corpus.getTagSet().size() == 0)
			throw new NullPointerException("Incorrect sentence or tag set");
		this.corpus = corpus;
		words = sentence;
		for (int i = 0; i < sentence.length; i++)
			words[i] = sentence[i].toLowerCase();
		populateTags();
		assignedTags = new String[words.length];
		assignedTagIndex = new int[words.length];
		score = new double[tags.length][words.length];
		backpointer = new int[tags.length][words.length];
	}

	/**
	 * Converts tag set from corpus into a <tt>String</tt> array
	 */
	private void populateTags() {
		tags = new String[corpus.getTagSet().size()];
		int i = 0;
		for (String tag : corpus.getTagSet()) 
			tags[i++] = tag;
	}

	/**
	 * Executes the Viterbi algorithm
	 */
	public void execute() {

		// Initialise
		for (int i = 0; i < tags.length; i++)
			score[i][0] = corpus.getTagProbability(tags[i], words[0]) * corpus.getBigramProbability(Tokeniser.START_TAG, tags[i]);
		
		// Induction
		for (int j = 1; j < words.length; j++) {
			for (int i = 0; i < tags.length; i++) {
				double max = 0;
				int backK = 0;
				for (int k = 0; k < tags.length; k++) {
					double value = score[k][j-1];
					value *= corpus.getBigramProbability(tags[k], tags[i]);
					value *= corpus.getTagProbability(tags[i], words[j]);
					if (max < value) {
						max = value;
						backK = k;
					}
				}
				score[i][j] = max;
				backpointer[i][j] = backK;
			}
//			System.out.println("Completed " + j*100/words.length + "%");
		}
		
		// Back tracing the best tagging
		double max = score[0][words.length-1];
		int tagIndex = 0;
		for (int i = 0; i < tags.length; i++) {
			double value = score[i][words.length-1];
			if (max < value) {
				max = value;
				tagIndex = i;
			}
		}
		assignedTagIndex[words.length-1] = tagIndex;
		for (int i = words.length-2; i >=0 ;i--)
			assignedTagIndex[i] = backpointer[assignedTagIndex[i+1]][i+1];
		
		convert();
	}
	
	/**
	 * Populates <tt>assignedTags</tt> based on values in <tt>assignedTagIndex</tt>
	 */
	private void convert() {
		for (int i = 0; i < assignedTagIndex.length; i++) {
			assignedTags[i] = tags[assignedTagIndex[i]];
		}
	}
	
	/**
	 * Returns the tags assigned to the given set of words by the Viterbi algorithm
	 * @return the tags assigned
	 */
	public String[] getAssignedTags() {
		return assignedTags;
	}
	
	/**
	 * Prints the assigned tags to the given set of words
	 */
	public void printTags() {
		for (String word : words)
			System.out.print(word + "\t");
		System.out.println();
		for (String tag : assignedTags)
			System.out.print(tag + "\t");
	}

}
