package part1;

import java.io.File;
import java.io.IOException;
import java.nio.file.NotDirectoryException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Given a directory containing POS data or a set of training files, provides methods to build word frequency
 * and tag frequency matrices.
 * 
 * @author Siddharth
 * 
 */
public class Corpus {
	
	// File object for the directory that contains POS data
	private File directory;

	// Set of files to build training corpus from
	private Set<File> trainFiles;
	
	// Set of tags
	private Set<String> tagSet;
	
	// Set of words in the corpus
	private Set<String> wordSet;
	
	// Map (2D associative array) of words against tags
	private Map<String, Map<String, Integer>> tagFrequency;
	
	// Map (2D associative array) of tags against tags
	private Map<String, Map<String, Integer>> bigramFrequency;
	
	// Total number of occurrences of a tag for all words
	// Used to calculate tagProbability
	private Map<String, Integer> totalTagFrequency;
	
	// Total number of occurrences of a tag for all tags
	// Used to calculate bigramProbability
	private Map<String, Integer> totalBigramFrequency;
		
	public Corpus(Set<File> files) throws NotDirectoryException {
		trainFiles = files;
		if (files == null || files.size() == 0)
			throw new NullPointerException("No training files received");
		tagFrequency = new HashMap<String, Map<String,Integer>>();
		bigramFrequency = new HashMap<String, Map<String,Integer>>();
		tagSet = new HashSet<String>();
		wordSet = new HashSet<String>();
		totalTagFrequency = new HashMap<String, Integer>();
		totalBigramFrequency = new HashMap<String, Integer>();
	}
	
	public Corpus(String dir) throws NotDirectoryException {
		directory = new File(dir);
		if (!directory.isDirectory())
			throw new NotDirectoryException(dir);
		tagFrequency = new HashMap<String, Map<String,Integer>>();
		bigramFrequency = new HashMap<String, Map<String,Integer>>();
		tagSet = new HashSet<String>();
		wordSet = new HashSet<String>();
		totalTagFrequency = new HashMap<String, Integer>();
		totalBigramFrequency = new HashMap<String, Integer>();
		trainFiles = getAllFiles(directory);
	}
	
	/**
	 * Gets all files recursively from the all sub-directories 
	 * of a given directory <tt>dir</tt> 
	 * @param dir the given directory
	 * @return <tt>Collection</tt> of <tt>Files</tt> object
	 */
	private Set<File> getAllFiles(File dir) {
		Set<File> fileTree = new HashSet<File>();
		for (File entry : dir.listFiles()) {
			if (entry.isFile()) 
				fileTree.add(entry);
			else 
				fileTree.addAll(getAllFiles(entry));
		}
		return fileTree;
	}
	
	/**
	 * Trains the corpus from all the training files as a N x M 
	 * matrix where N is the number of words and M is the number of tags.
	 * @throws IOException if the file is not found or cannot be read
	 */
	public void train() throws IOException {
		
		for (File file : trainFiles) {
			Tokeniser tokeniser = new Tokeniser(file);
			tokeniser.tokenise();
			List<String> words = tokeniser.getWords();
			List<String> tags = tokeniser.getTags();

			for (int i = 0; i < words.size(); i++) {
				
				// Add tag to tagSet
				if (!tagSet.contains(tags.get(i)))
					tagSet.add(tags.get(i));

				// Add word to wordSet
				if (!wordSet.contains(words.get(i)))
					wordSet.add(words.get(i));
				
				// Increase value of <word><tag> pair by 1
				if (!tagFrequency.containsKey(words.get(i))) {
					Map<String, Integer> internalMap = new HashMap<String, Integer>();
					internalMap.put(tags.get(i), 1);
					tagFrequency.put(words.get(i), internalMap);
				}
				else {
					Map<String, Integer> internalMap = tagFrequency.get(words.get(i));
					if (!internalMap.containsKey(tags.get(i)))
						internalMap.put(tags.get(i), 1);
					else
						internalMap.put(tags.get(i), internalMap.get(tags.get(i)) + 1);
					tagFrequency.put(words.get(i), internalMap);
				}
			}
			populateBigramFrequency(tags);
		}
		fillZeroes();
		populateTotalTagFrequency();
		populateTotalBigramFrequency();
	}
	
	/**
	 * Populates bigram frequency <tt>Map</tt> from a given <tt>List</tt> of tags
	 * @param tags given <tt>List</tt> of tags
	 */
	private void populateBigramFrequency(List<String> tags) {
		for (int i = 0; i < tags.size()-1; i++) {
			// Increase value of <tag><next Tag> pair by 1
			if (!bigramFrequency.containsKey(tags.get(i))) {
				Map<String, Integer> internalMap = new HashMap<String, Integer>();
				internalMap.put(tags.get(i+1), 1);
				bigramFrequency.put(tags.get(i), internalMap);
			}
			else {
				Map<String, Integer> internalMap = bigramFrequency.get(tags.get(i));
				if (!internalMap.containsKey(tags.get(i+1)))
					internalMap.put(tags.get(i+1), 1);
				else
					internalMap.put(tags.get(i+1), internalMap.get(tags.get(i+1)) + 1);
				bigramFrequency.put(tags.get(i), internalMap);
			}
		}
	}
	
	/**
	 * Fills missing word-tag pair with zero.
	 * Should only be called after <tt>populate()</tt> and <tt>populateBigramFrequency(tags)</tt>
	 * 
	 */
	private void fillZeroes() {
		for (Map.Entry<String, Map<String, Integer>> entry : tagFrequency.entrySet()) {
			Map<String, Integer> internalMap = entry.getValue();
			for (String tag : tagSet) {
				if (!internalMap.containsKey(tag))
					internalMap.put(tag, 1);
				else
					internalMap.put(tag, internalMap.get(tag) + 1);
			}
		}
		
		for (Map.Entry<String, Map<String, Integer>> entry : bigramFrequency.entrySet()) {
			Map<String, Integer> internalMap = entry.getValue();
			// Add 1 smoothing
			for (String tag : tagSet) {
				if (!internalMap.containsKey(tag))
					internalMap.put(tag, 1);
				else
					internalMap.put(tag, internalMap.get(tag) + 1);
			}
		}
	}
	
	/**
	 * Populates <tt>totalTagFrequency</tt> based on <tt>tagFrequency</tt>
	 * Should only be called after <tt>tagFrequency</tt>, <tt>tagSet</tt> and 
	 * <tt>wordSet</tt> has been calculated
	 */
	private void populateTotalTagFrequency() {
		for (String tag: tagSet) {
			int total = 0;
			for (String someWord : wordSet)
				total += getTagFrequency(someWord, tag);
			totalTagFrequency.put(tag, total);
		}
	}
	
	/**
	 * Populates <tt>totalBigramFrequency</tt> based on <tt>bigramFrequency</tt>
	 * Should only be called after <tt>bigramFrequency</tt> and <tt>tagSet</tt> 
	 * has been calculated
	 */
	private void populateTotalBigramFrequency() {
		for (String tag : tagSet) {
			int total = 0;
			for (String someTag : tagSet)
				total += getBigramFrequency(tag, someTag);
			totalBigramFrequency.put(tag, total);
		}
	}
	
	/**
	 * Returns the number of occurrences of a given <tt>nextTag<tt> after a <tt>tag</tt>
	 * @param tag category of tag appearing first
	 * @param nextTag category of tag appearing after <tt>tag</tt>
	 * @return number of occurrences
	 */
	public Integer getBigramFrequency(String tag, String nextTag) {
//		if (!tagSet.contains(tag))
//			return new Integer(Integer.MIN_VALUE);
//		if (!tagSet.contains(nextTag))
//			return new Integer(Integer.MIN_VALUE);
//		if (!bigramFrequency.containsKey(tag))
//			return new Integer(Integer.MIN_VALUE);
//		if (!bigramFrequency.get(tag).containsKey(nextTag))
//			return new Integer(Integer.MIN_VALUE);
		return bigramFrequency.get(tag).get(nextTag);
	}
	
	/**
	 * Returns the number of occurrences of a given words as a given tag
	 * @param word the given word
	 * @param tag the given tag
	 * @return number of occurrences
	 */
	public Integer getTagFrequency(String word, String tag) {
		if (!wordSet.contains(word))
			return new Integer(Integer.MIN_VALUE);
//		if (!tagSet.contains(tag))
//			return new Integer(Integer.MIN_VALUE);
//		if (!tagFrequency.containsKey(word))
//			return new Integer(Integer.MIN_VALUE);
//		if (!tagFrequency.get(word).containsKey(tag))
//			return new Integer(Integer.MIN_VALUE);
		return tagFrequency.get(word).get(tag);
	}
	
	/**
	 * Returns the conditional probability P(word|tag)
	 * @param word the given word
	 * @param tag the given tag
	 * @return the conditional probability
	 */
	public double getTagProbability(String tag, String word) {
		double probability = 0;
		probability = Math.log(getTagFrequency(word, tag)) - Math.log(totalTagFrequency.get(tag));
		probability = Math.pow(Math.E, probability);
		return probability;
	}
	
	/**
	 * Returns the conditional probability P(nextTag|tag)
	 * @param tag the first tag
	 * @param nextTag the subsequent tag
	 * @return the conditional probability
	 */
	public double getBigramProbability(String tag, String nextTag) {
		double probability = 0;
		probability = Math.log(getBigramFrequency(tag, nextTag)) - Math.log(totalBigramFrequency.get(tag));
		probability = Math.pow(Math.E, probability);
		return probability;
	}
	
	/**
	 * Returns the tag set of the corpus
	 * @return the tag set
	 */
	public Set<String> getTagSet() {
		return tagSet;
	}

}
