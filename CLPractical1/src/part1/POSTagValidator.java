package part1;

import java.io.File;
import java.io.IOException;
import java.nio.file.NotDirectoryException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Creates corpus and validates POS tag using cross-validation
 * 
 * @author Siddharth
 *
 */
public class POSTagValidator {
	
	// File object for the directory that contains POS data
	private File directory;
	
	// Set of files for training
	private Set<File> trainSetFiles;
	
	// Set of files for testing
	private Set<File> testSetFiles;
	
	// Given argument for validation
	private String argument;
	
	// Value of k for k-fold cross-validation
	private static final int K = 10;
	
	// String array of allowed arguments
	private static final String[] ARGS = {"-full", "-cross"};

	public POSTagValidator(String dir, String arg) throws NotDirectoryException {
		if (!Arrays.asList(ARGS).contains(arg))
			throw new IllegalArgumentException("Illegal argument: " + arg);
		this.argument = arg;

		directory = new File(dir);
		if (!directory.isDirectory())
			throw new NotDirectoryException(dir);
		trainSetFiles = new HashSet<File>();
		testSetFiles = new HashSet<File>();
	}
	
	/**
	 * Validates Viterbi algorithm on given set of data using either full training set
	 * or using k-fold cross validation with the given <tt>K</tt> value
	 * @throws IOException 
	 */
	public void validate() throws IOException {
		if (argument.equals(ARGS[0]))
			validateFull();
		else
			validateCross();
	}

	/**
	 * Validates using k-fold cross validation
	 * @throws IOException if the files are not found or cannot be read
	 */
	private void validateCross() throws IOException {
		Set<File> fileSet = getAllFiles(directory);
		File[] listOfFiles = fileSet.toArray(new File[fileSet.size()]);
		double accuracy = 0;
		int testIndexStart = 0;
		int testSize = listOfFiles.length/K;
		for (int runs = 0; runs < K; runs++) {
			for (int i = 0; i < listOfFiles.length; i++) {
				if (i >= testIndexStart && i < (testIndexStart+testSize))
					testSetFiles.add(listOfFiles[i]);
				else
					trainSetFiles.add(listOfFiles[i]);
			}
			Corpus c = new Corpus(trainSetFiles);
			c.train();
			System.out.println("Created training corpus");
			TestSentence testSentence = getTestSentence();
			Viterbi v = new Viterbi(testSentence.getSentence(), c);
			System.out.println("Executing Viterbi");
			v.execute();
			System.out.println("Executed Viterbi, computing accuracy");
			String[] assignedTags = v.getAssignedTags();
			String[] actualTags = testSentence.getActualTags();
			double accuracyThisRun = (double)computeAccuracy(actualTags, assignedTags)*100*(K-1)/((double)actualTags.length*K);
			accuracy += accuracyThisRun;
			
			testIndexStart += testSize;
			System.out.println("Completed " + (runs+1) + "-th run");
			System.out.println("Accuracy of this run is " + accuracyThisRun + "%");
		}
		accuracy /= K;
		System.out.println("Total accuracy of all runs " + accuracy + "%");
	}

	/**
	 * Validates using the entire corpus as the training set
	 * @throws IOException if the files are not found or cannot be read
	 */
	private void validateFull() throws IOException {
		trainSetFiles = getAllFiles(directory);
		File[] listOfFiles = trainSetFiles.toArray(new File[trainSetFiles.size()]);
		Corpus c = new Corpus(trainSetFiles);
		c.train();
		System.out.println("Created training corpus");
		
		double accuracy = 0;
		int testIndexStart = 0;
		int testSize = listOfFiles.length/K;
		for (int runs = 0; runs < K; runs++) {
			for (int i = 0; i < listOfFiles.length; i++) {
				if (i >= testIndexStart && i < (testIndexStart+testSize))
					testSetFiles.add(listOfFiles[i]);
			}
			TestSentence testSentence = getTestSentence();
			Viterbi v = new Viterbi(testSentence.getSentence(), c);
			System.out.println("Executing Viterbi");
			v.execute();
			System.out.println("Executed Viterbi, computing accuracy");
			String[] assignedTags = v.getAssignedTags();
			String[] actualTags = testSentence.getActualTags();
			double accuracyThisRun = (double)computeAccuracy(actualTags, assignedTags)*100/(double)actualTags.length;
			accuracy += accuracyThisRun;
			
			testIndexStart += testSize;
			System.out.println("Completed " + (runs+1) + "-th run");
			System.out.println("Accuracy of this run is " + accuracyThisRun + "%");
		}
		accuracy /= K;
		System.out.println("Total accuracy of all runs " + accuracy + "%");
	}
	
	/**
	 * Computes the number of correctly assigned tags 
	 * @param actualTags the actual tags of a sentence
	 * @param assignedTags the assigned tags of a sentence
	 * @return the number of correct tags
	 */
	private int computeAccuracy(String[] actualTags, String[] assignedTags) {
		if (actualTags.length != assignedTags.length)
			System.out.println("Incorrect tags length!!!");
		
		int correct = 0;
		for (int i = 0; i < actualTags.length; i++)
			if (!actualTags[i].equalsIgnoreCase(assignedTags[i]))
				correct++;
		return correct;
	}
	
	/**
	 * Creates and returns a <tt>TestSentence</tt> instance from the words and tags in the
	 * <tt>testSetFiles</tt>. Re-initialises <tt>testSetFiles</tt>
	 * @return the <tt>TestSentence</tt>
	 * @throws IOException if the test file is not found or cannot be read
	 */
	private TestSentence getTestSentence() throws IOException {
		List<String> sentence = new ArrayList<String>();
		List<String> assignedTags = new ArrayList<String>();
		for (File file : testSetFiles) {
			Tokeniser tokeniser = new Tokeniser(file);
			tokeniser.tokenise();
			List<String> words = tokeniser.getWords();
			List<String> tags = tokeniser.getTags();
			List<String> newSentence = new ArrayList<String>(sentence);
			newSentence.addAll(words);
			sentence = newSentence;
			
			List<String> newAssignedTags = new ArrayList<String>(assignedTags);
			newAssignedTags.addAll(tags);
			assignedTags = newAssignedTags;
		}
		testSetFiles = new HashSet<File>();
		return new TestSentence(sentence, assignedTags);
	}

	/**
	 * Gets all files recursively from the all sub-directories 
	 * of a given directory <tt>dir</tt> 
	 * @param dir the given directory
	 * @return <tt>Set</tt> of <tt>Files</tt> object
	 */
	private Set<File> getAllFiles(File dir) {
		Set<File> fileTree = new HashSet<File>();
		for (File entry : dir.listFiles()) {
			if (entry.isFile()) 
				fileTree.add(entry);
			else 
				fileTree.addAll(getAllFiles(entry));
		}
		return fileTree;
	}
}
