package part1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Given either <tt>File</tt> or <tt>String</tt>, <tt>Tokeniser</tt> provides methods to tokenise the given 
 * words into tags specified by Eric Brill's part-of-speech tagger. 
 * 
 * @author Siddharth
 * 
 */
public class Tokeniser {
	
	// Dummy start word
	public static final String START_WORD = "^^START^^";
	
	// Dummy start tag
	public static final String START_TAG = "£££";
	
	// Convention for end tag. We use full stop to indicate end of sentence
	public static final String END_TAG = ".";
	
	// Given file to be tagged
	private File file;
	
	// Given text to be tagged. Either File or String should be given
	// If File is given, each line is iteratively treated as text 
	private String text;
	
	// List of words in file or String
	private List<String> words;
	
	// Corresponding tags for given words
	private List<String> tags;

	// Regular expression used to separate tags and words
	private String regex;
	
	public Tokeniser(String text) {
		this.text = text;
		this.tags = new ArrayList<String>();
		this.words = new ArrayList<String>();
		this.regex = "[\\w\\d!%&#$.,;:{}()\"`'-\\\\/]*/[A-Z#$.,:()\"`']*";
	}
	
	public Tokeniser(File file) {
		this.file = file;
		this.tags = new ArrayList<String>();
		this.words = new ArrayList<String>();
		this.regex = "[\\w\\d!%&#$.,;:{}()\"`'-\\\\/]*/[A-Z#$.,:()\"`']*";
	}

	/**
	 * Tokenises the words of given <tt>File</tt> or <tt>String</tt> into tags
	 * @throws IOException if the file is not found or cannot be read
	 */
	public void tokenise() throws IOException {
//		System.out.println("Tokenising: " + file.getAbsolutePath());
		if (file != null) {
			BufferedReader br = new BufferedReader(new FileReader(file));
			while ((text = br.readLine()) != null)
				tokeniseAndPopulate();
			br.close();
			return;
		}
		else if (text != null) {
			tokeniseAndPopulate();
		}
		else
			throw new NullPointerException("No file or text to tokenise");
	}
	
	/**
	 * Tokenises the words of given <tt>File</tt> or <tt>String</tt> into tags
	 * without adding <tt>START_TAG</tt> and <tt>START_WORD</tt> tokens after the <tt>END_TAG</tt>
	 * @throws IOException if the file is not found or cannot be read
	 */
	public void tokeniseAll() throws IOException {
//		System.out.println("Tokenising: " + file.getAbsolutePath());
		if (file != null) {
			BufferedReader br = new BufferedReader(new FileReader(file));
			while ((text = br.readLine()) != null)
				tokeniseAndPopulateWithoutDummyTokens();
			br.close();
			return;
		}
		else if (text != null) {
			tokeniseAndPopulateWithoutDummyTokens();
		}
		else
			throw new NullPointerException("No file or text to tokenise");
	}
	
	/**
	 * Tokenises just one line of text and populates it in the
	 * <tt>tags</tt> and <tt>words</tt> <tt>List</tt>
	 */
	private void tokeniseAndPopulate() {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(text);
		
		while (matcher.find()) {
			String[] subString = matcher.group().split("/");
			if (subString.length < 2)
				throw new PatternSyntaxException("Error in text: " + text + " Matched: " + matcher.group(), regex, -1);
			else {
				String str = joinSubStrings(subString);
				String tag = subString[subString.length-1];
				String[] subTags = tag.split("\\|");
				// To account for the case when a word may have two possible tags in the text
				// Example, word/<tag1>|<tag2>
				// Solution: add both tags to same word
				for (String subTag : subTags) {
					words.add(str.toLowerCase());
					tags.add(subTag);
				}
				if (str == "")
					System.out.println("NO WORD MATCHED - DANGER!!!!!!!!!!!!!!!!!!!!!! " + file.getAbsolutePath() + "   " + subString[1]);
				
				// If the current tag is END_TAG, mark next tag as start tag
				if (subString[subString.length-1].equals(END_TAG)) {
					words.add(START_WORD);
					tags.add(START_TAG);
				}
			}
		}
	}
	
	/**
	 * Tokenises just one line of text and populates it in the
	 * <tt>tags</tt> and <tt>words</tt> <tt>List</tt>. Does not add 
	 * <tt>START_TAG</tt> and <tt>START_WORD</tt> after the <tt>END_TAG</tt>
	 */
	private void tokeniseAndPopulateWithoutDummyTokens() {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(text);
		
		while (matcher.find()) {
			String[] subString = matcher.group().split("/");
			if (subString.length < 2)
				throw new PatternSyntaxException("Error in text: " + text + " Matched: " + matcher.group(), regex, -1);
			else {
				String str = joinSubStrings(subString);
				String tag = subString[subString.length-1];
				String[] subTags = tag.split("\\|");
				// To account for the case when a word may have two possible tags in the text
				// Example, word/<tag1>|<tag2>
				// Solution: add both tags to same word
				for (String subTag : subTags) {
					words.add(str);
					tags.add(subTag);
				}
				if (str == "")
					System.out.println("NO WORD MATCHED - DANGER!!!!!!!!!!!!!!!!!!!!!! " + file.getAbsolutePath() + "   " + subString[1]);
			}
		}
	}
	
	/**
	 * If <tt>subString</tt> contains a fraction of the form x/\y/TAG,
	 * this method returns x/y as the word, and last <tt>subString</tt> can be 
	 * used as the tag
	 * @param subString
	 * @return
	 */
	private String joinSubStrings(String[] subString) {
		String str = "";
		for (int i = 0; i < subString.length-1; i++) {
			str = str.concat(subString[i]);
			if (i < subString.length-1-1)
				str = str.concat("/");
		}
		return str.replace("\\", "");
	}
	
	/**
	 * Returns the <tt>List</tt> of tags in the current text of file
	 * @return an <tt>List</tt> of tags
	 */
	public List<String> getTags() {
		return tags;
	}

	/**
	 * Returns the <tt>List</tt> of words in the current text of file
	 * @return a <tt>List</tt> of words
	 */
	public List<String> getWords() {
		return words;
	}

}
